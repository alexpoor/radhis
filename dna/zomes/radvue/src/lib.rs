use hdk::prelude::*;

mod health_data;
mod utils;
mod audit_data;
mod capgrant;
mod profile;

use health_data::{HealthDataInput, HealthDataOutput, HealthDataRemoteInput, HealthData};
use audit_data::{AuditDataOutput};
use capgrant::{GrantCapAccess,HeaderAndEntryHash,CapReceive,CapRecordOutput};
use profile::{ProfileInput, ProfileOutput};

entry_defs![
    Path::entry_def(),
    health_data::HealthData::entry_def(),
    health_data::HealthDataWrapper::entry_def(),
    audit_data::AuditData::entry_def(),
    audit_data::AuditDataWrapper::entry_def(),
    profile::Profile::entry_def(),
    capgrant::CapRecord::entry_def()
];

pub fn err(reason: &str) -> WasmError {
    WasmError::Guest(String::from(reason))
}

// cap token stuff
#[hdk_extern]
pub fn init(_: ()) -> ExternResult<InitCallbackResult> {
    // grant unrestricted access to accept_cap_claim so other agents can send us claims
    let mut functions: GrantedFunctions = BTreeSet::new();
    functions.insert((zome_info()?.name, "receive_cap_access".into()));
    create_cap_grant(CapGrantEntry {
        tag: "".into(),
        // empty access converts to unrestricted
        access: ().into(),
        functions,
    })?;

    Ok(InitCallbackResult::Pass)
}

#[hdk_extern]
pub fn create_capgrant(access: GrantCapAccess) -> ExternResult<HeaderAndEntryHash> {
    capgrant::create_capgrant(access)
}

// create health_data
#[hdk_extern]
pub fn create_health_data(health_data_input: HealthDataInput,) -> ExternResult<HealthDataOutput> {
    health_data::create_health_data(health_data_input)
}

// create profile
#[hdk_extern]
pub fn create_profile(profile_input: ProfileInput) -> ExternResult<ProfileOutput> {
    profile::create_profile(profile_input)
}

// list all health_data entries
#[hdk_extern]
pub fn list_health_data(_: ()) -> ExternResult<Vec<HealthDataOutput>> {
    let health_data = health_data::list_health_data()?;
    
    Ok(health_data)
}

// list profiles
#[hdk_extern]
pub fn list_profiles(_: ()) -> ExternResult<Vec<ProfileOutput>> {
    let profiles = profile::list_profiles()?;

    Ok(profiles)
}

// list caprecords
#[hdk_extern]
pub fn list_caprecord(_: ()) -> ExternResult<Vec<CapRecordOutput>> {
    let caprecords = capgrant::list_caprecord()?;

    Ok(caprecords)
}

// list all audit_data entries
#[hdk_extern]
pub fn list_audit_data(_: ()) -> ExternResult<Vec<AuditDataOutput>> {
    let audit_data = audit_data::list_audit_data()?;

    Ok(audit_data)
}

#[hdk_extern]
pub fn receive_cap_access(cap: CapReceive) -> ExternResult<()> {
    capgrant::receive_cap_access(cap)
}


#[hdk_extern]
pub fn get_cap_tokens(filter_by_agent: AgentPubKey) -> ExternResult<CapClaim> {
    capgrant::get_cap_tokens(filter_by_agent)
}

#[hdk_extern]
fn call_list_cap_claim(filter_by_agent: AgentPubKey) -> ExternResult<Vec<HealthDataOutput>> {
    capgrant::call_list_cap_claim(filter_by_agent)
}

#[hdk_extern]
fn call_create_cap_claim(health_data_input: HealthDataRemoteInput) -> ExternResult<HealthDataOutput> {
    capgrant::call_create_cap_claim(health_data_input)
}

#[hdk_extern]
pub fn list_health_data_with_capgrant(pubkey: AgentPubKey) -> ExternResult<Vec<HealthDataOutput>> {
    let health_data = health_data::list_health_data_with_capgrant(pubkey)?;
    
    Ok(health_data)
}

// delete CapGrant using HeaderHash
#[hdk_extern]
pub fn delete_cap_grant(header_hash: HeaderHash) -> ExternResult<HeaderHash> {
    hdk::prelude::delete_cap_grant(header_hash)
}

// creates a health_data entry via CapGrant AND create an audit-data entry
#[hdk_extern]
pub fn create_health_data_with_capgrant(health_data: HealthData) -> ExternResult<HealthDataOutput> {
    let health_data = health_data::create_health_data_with_capgrant(health_data)?;
    
    Ok(health_data)
}