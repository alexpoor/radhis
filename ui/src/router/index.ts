import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import CreateProfile from '@/components/CreateProfile.vue'
import LoggedIn from '@/views/LoggedIn.vue'
import Access from '@/views/Access.vue'
import Audit from '@/views/Audit.vue'

const routes: Array<RouteRecordRaw> = [
  // users land on create profile page
  {
  path: '/',
  name: 'CreateProfile',
  component: CreateProfile
  },
  {
    path: '/radhis',
    name: 'LoggedIn',
    component: LoggedIn
  },
  {
    path: '/access',
    name: 'Access',
    component: Access
  },
  {
    path: '/audit',
    name: 'Audit',
    component: Audit
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router