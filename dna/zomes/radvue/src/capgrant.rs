use crate::health_data;
use crate::utils;

// use hc_utils::{WrappedAgentPubKey};
use holo_hash::{AgentPubKey};
use hdk::prelude::*;


#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GrantCapAccess {
    pub function: String,
    pub agent: AgentPubKey
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct HeaderAndEntryHash {
    pub header_hash: HeaderHash,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CapReceive {
    pub cap_secret: CapSecret,
    pub from_agent: AgentPubKey
}

#[hdk_entry(id = "cap_record", visibility = "private")]
#[serde(rename_all = "camelCase")]
#[derive(Clone)]
pub struct CapRecord {
    pub timestamp: String,
    pub grantee: AgentPubKey,
    pub function: String,
    pub headerhash: HeaderHash,
    pub active: bool
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct CapRecordOutput {
   entry_hash: EntryHash,
   entry: CapRecord,
}


// START CAPGRANT BLOCK
pub fn receive_cap_access(cap: CapReceive) -> ExternResult<()> {
    create_cap_claim(CapClaimEntry::new(
        "p2p_message".into(),
        cap.from_agent,
        cap.cap_secret,
    ))?;
    Ok(())
}

// creates assigned cap grant for users to access specified remote function
pub fn create_capgrant(access: GrantCapAccess) -> ExternResult<HeaderAndEntryHash> {
    // Create function map of cap grant
    // let mut functions: GrantedFunctions = HashSet::new();
    let mut functions: GrantedFunctions = BTreeSet::new();
    let this_zome = zome_info()?.name;
    // remote function to access is passed from GrantCapAccess input
    functions.insert((this_zome.clone(), access.function.clone().into()));

    debug!("### create_capgrant GrantedFunctions: {:#?}", functions);

    // Create the cap grant and commit for current agent
    let cap_secret = generate_cap_secret()?;

    let a: HeaderHash = create_cap_grant(CapGrantEntry {
        tag: "cap_grant".into(),
        access: CapAccess::from((cap_secret, access.agent.clone())), // grantee
        functions,
    })?;
    let result = HeaderAndEntryHash {
        header_hash: a.clone(),
    };


    // ## write a capgrant entry into CapRecord
    // These entries would never be shared so we don't need the wrapper workflow
    let now = sys_time()?;

    let cap_input = CapRecord {
        grantee: access.agent.clone(),
        timestamp: now.to_string(),
        function: access.function.clone(),
        headerhash: a.clone(),
        active: true
    };

    create_entry(&cap_input)?;
 
    let cap_input_hash = hash_entry(&cap_input)?;

    let path = caprecord_path(AgentPubKey::from(agent_info()?.agent_latest_pubkey.clone()));

    path.ensure()?;

    create_link(path.hash()?, cap_input_hash.clone(), ())?;

    // ## end capgrant entry


    // Call the zome of target agent and give them the generated cap secret
    call_remote(
        access.agent.clone(),
        zome_info()?.name,
        "receive_cap_access".into(),
        None,
        CapReceive {
            cap_secret: cap_secret,
            from_agent: agent_info()?.agent_initial_pubkey
        },
    )?;
    Ok(result)
}

pub fn get_cap_tokens(filter_by_agent: AgentPubKey) -> ExternResult<CapClaim> {
    let cap_claims = query(
        ChainQueryFilter::new()
            .entry_type(EntryType::CapClaim)
            .include_entries(true),
        )?
        .into_iter()
        .map(|elem| {
            elem.entry()
                .to_owned()
                .into_option()
                .unwrap()
                .as_cap_claim()
                .expect("Could not get option")
                .to_owned()
        })
        .filter(|grant| grant.grantor() == &filter_by_agent)
        .collect::<Vec<CapClaim>>()
        .pop()
        .ok_or(WasmError::Host(
            "No cap claim was found for desired target agent".to_string(),
        ))?
        ;
    debug!("### get_cap_tokens Got claims: {:#?}", cap_claims);
    Ok(cap_claims)
}


// Workflow for capgrant CREATE and LIST is different so we need different functions/workflow here
pub fn call_list_cap_claim(filter_by_agent: AgentPubKey) -> ExternResult<Vec<health_data::HealthDataOutput>> {
    let cap_tokens = get_cap_tokens(filter_by_agent)?;
    let cap_receive = CapReceive { cap_secret: *cap_tokens.secret(), from_agent: cap_tokens.grantor().clone()};
    let agent_info = agent_info()?;
    let pubkey = AgentPubKey::from(agent_info.agent_latest_pubkey.clone());
    debug!("### call_list_cap_claim pubkey: {:#?}",pubkey);
    health_data::list_health_data_remote(cap_receive,pubkey)
}


pub fn call_create_cap_claim(health_data_input: health_data::HealthDataRemoteInput) -> ExternResult<health_data::HealthDataOutput> {
    let cap_tokens = get_cap_tokens(health_data_input.filter_by_agent)?;
    let cap_receive = CapReceive { cap_secret: *cap_tokens.secret(), from_agent: cap_tokens.grantor().clone()};
    let agent_info = agent_info()?;
    let pubkey = AgentPubKey::from(agent_info.agent_latest_pubkey.clone());
    debug!("### call_create_cap_claim pubkey: {:#?}",pubkey);

    // stitch HealthDataInput and pubkey into new struct to send to create_health_data_remote
    let health_data_remote_input = health_data::HealthData {
        created_by: pubkey,
        content: health_data_input.content,
        resource_type: health_data_input.resource_type,
        start_time: health_data_input.start_time,
        end_time: health_data_input.end_time
    };
    debug!("### health_data_remote_input contents: {:#?},{:#?},{:#?}",health_data_remote_input.created_by,health_data_remote_input.content,health_data_remote_input.resource_type);

    health_data::create_health_data_remote(cap_receive,health_data_remote_input)
}

// returns all caprecord entries
pub fn list_caprecord() -> ExternResult<Vec<CapRecordOutput>> {
    let path = caprecord_path(AgentPubKey::from(agent_info()?.agent_latest_pubkey.clone()));
    debug!("##################LIST_HEALTH_DATA_PATH{:?}",path);
    let links = get_links(path.hash()?, None)?;
    debug!("##################LIST_HEALTH_DATA_LINKS{:?}",links);

    let mut results = vec![];
    for a in links
        .iter()
        {
            let caprecord = utils::try_get_and_convert::<CapRecord>(HoloHash::from(a.target.clone()))?;
            debug!("##################try get and convert3: {:?}",caprecord);
            results.push(CapRecordOutput{
                entry_hash:HoloHash::from(a.target.clone()),
                entry:caprecord
            })
        }
        return Ok(results);
}

// Private helpers - anchors for the path
pub fn caprecord_path(key: AgentPubKey) -> Path {
    Path::from(format!("caprecord-{}",key))
}