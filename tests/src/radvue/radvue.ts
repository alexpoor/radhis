
import { Orchestrator, Player, Cell } from "@holochain/tryorama";
import { config, installation, sleep } from '../utils';

export default (orchestrator: Orchestrator<any>) => 
  orchestrator.registerScenario("radvue tests", async (s, t) => {
    // Declare players using the previously specified config, nicknaming them
    // note that the first argument to players is just an array conductor configs that will
    // be used to spin up the conductor processes which are returned in a matching array.
    const [alice_player, bob_player, carol_player]: Player[] = await s.players([config, config, config]);

    // install your happs into the conductors and destructuring the returned happ data using the same
    // array structure as you created in your installation array.
    const [[alice_happ]] = await alice_player.installAgentsHapps(installation);
    const [[bob_happ]] = await bob_player.installAgentsHapps(installation);
    const [[carol_happ]] = await carol_player.installAgentsHapps(installation);

    await s.shareAllNodes([alice_player, bob_player, carol_player]);

    const alice = alice_happ.cells.find(cell => cell.cellRole.includes('/radvue.dna')) as Cell;
    const bob = bob_happ.cells.find(cell => cell.cellRole.includes('/radvue.dna')) as Cell;
    const carol = carol_happ.cells.find(cell => cell.cellRole.includes('/radvue.dna')) as Cell;

    // counter to number tests automatically  
    let testcount = 0;

    // alice creates profile
    const aprofile = await alice.call(
      "radvue",
      "create_profile",
      {
        nickname: "Alice"
      }
    );

    console.log("PROFILE");
    console.log(aprofile);

    // bob creates profile
    const bprofile = await bob.call(
      "radvue",
      "create_profile",
      {
        nickname: "Bob"
      }
    );

    console.log("PROFILE");
    console.log(bprofile);

    // carol creates profile
    const cprofile = await carol.call(
      "radvue",
      "create_profile",
      {
        nickname: "Carol"
      }
    );

    console.log("PROFILE");
    console.log(cprofile);

    await sleep(50);

    // list profiles
    const blistprofile = await bob.call(
      "radvue",
      "list_profiles",
      null,
    );

    console.log("LIST PROFILES");
    console.log(blistprofile);


    // alice creates health data
    console.log("Make a health data entry for Alice...")
    const alice_result_1 = await alice.call(
    "radvue",
    "create_health_data",
    {
      content: "Alice #1",
      resourceType: "Primary care",
      startTime: 1629973116813,
      endTime: 1629973116813
    }
  );
  // test #0 - valid result for Alice's entry
  testcount++;
  // t.ok(alice_result_1,"TEST: #",+testcount+": Alice made a successful health data entry");
  t.ok(alice_result_1,testcount+": Alice made a successful health data entry");
  
    console.log("Now we are making two more health data entries for Alice...")
    const alice_result_2 = await alice.call(
    "radvue", 
    "create_health_data", 
    {
      content: "Alice #2",
      resourceType: "Respiratory",
      startTime: 1629973116813,
      endTime: 1629973116813
    }
  );

  const alice_result_3 = await alice.call(
    "radvue", 
    "create_health_data", 
    {
      content: "Alice #3",
      resourceType: "Respiratory",
      startTime: 1629973116813,
      endTime: 1629973116813
    }
  );

  // console.log(alice_result_1);

  await sleep(500);

    // get alice to list all her entries
  const list_health_data_alice = await alice.call(
    "radvue",
    "list_health_data",
    null
  );

    // test #2 - can we get all 3 entries back?
  console.log("LIST HEALTH DATA", list_health_data_alice.length)
  console.log(list_health_data_alice);
  testcount++;
  t.equal(list_health_data_alice.length, 3, testcount+": We get exactly 3 entries back for Alice");

  // check audit_data
  // get bob to list all their audit entries
  console.log("CHECK AUDIT DATA")
  let list_audit_data_bob = await bob.call(
    "radvue",
    "list_audit_data",
    null
  );
  console.log(list_audit_data_bob); // should be empty
  // test #5 - is length of Bob AuditData entries 0?
  testcount++;
  t.equal(list_audit_data_bob.length, 0, testcount+": Bob has 0 Audit Data entries");


  // now do some cap grant stuff
  // Bob is giving Alice capGrant to access list_health_data
  console.log("CAPABILITY GRANT")
  console.log("Now Bob is going to give Alice delegation to access his data...")

  let capAssignedGrant = await bob.call(
    "radvue",
    "create_capgrant",
    {
      function: "list_health_data_with_capgrant",
      agent: alice_happ.agent,
    }
  );
  console.log("alice_happ.agent: ");
  console.log(alice_happ.agent);

  // test #6 - do we have a valid cap grant?
  testcount++;
  t.ok(capAssignedGrant, testcount+": Alice has a valid capability grant");

  console.log("Now make two health data entries for Bob...")
  const bob_result_1 = await bob.call(
    "radvue",
    "create_health_data",
    {
      content: "Bob #1",
      resourceType: "Cardiology",
      startTime: 1629973116813,
      endTime: 1629973116813
    }
  );

  const bob_result_2 = await bob.call(
    "radvue",
    "create_health_data",
    {
      content: "Bob #2",
      resourceType: "Rheumatology",
      startTime: 1629973116813,
      endTime: 1629973116813
    }
  );

  console.log("Now make one entry for Carol...");
  const carol_result_1 = await carol.call(
    "radvue",
    "create_health_data",
    {
      content: "Carol #1",
      resourceType: "ENT",
      startTime: 1629973116813,
      endTime: 1629973116813
    }
  );

  await sleep(500);

  // get bob to list all entries
  const list_health_data_bob = await bob.call(
    "radvue",
    "list_health_data",
    null
  );

  // get carol to list all entries
  console.log("CAROL_LIST_DATA_1");
  const list_health_data_carol = await carol.call(
    "radvue",
    "list_health_data",
    null
  );
  console.log("LIST_HEALTH_DATA_CAROL", list_health_data_carol.length);

  console.log(list_health_data_carol);

    // test #3 - can we get all 2 entries back?
  testcount++;
  t.equal(list_health_data_bob.length, 2, testcount+": We get exactly 2 entries back for Bob");

  await sleep(500);

  console.log("Now Alice is going to use that token to get Bob's entries...")
  let TokenResults = await alice.call(
    "radvue",
    "call_list_cap_claim",
    bob_happ.agent
  );

  console.log("List the results of Alice's call using the capability token...")
  // log response from remote call
  console.log(TokenResults);
  // test #8 - do we have a valid response?
  testcount++;
  t.ok(TokenResults,testcount+": Alice has a valid response from CapGrant");

    // get bob to list all their audit entries
  console.log("Now get Bob to list his audit data entries again...")
  let list_audit_data_bob2 = await bob.call(
    "radvue",
    "list_audit_data",
    null
  );
  console.log(list_audit_data_bob2);

  console.log("Bob has ["+list_audit_data_bob2.length+"] audit data entries")

  // test #9 - there should be exactly one audit data entry
  testcount++;
  t.equal(list_audit_data_bob2.length,1,testcount+": Bob has 1 Audit Data entry currently");

  // now we need to build a test for delete_cap_grant
  // function requires HeaderHash as input
  // get the HeaderHash of Bob's CapGrant so we can use it later to delete the entry
  let bob_grant_header = capAssignedGrant.header_hash;
  
  console.log("REVOKE CAPABILITY GRANT")
  console.log("Now Bob is going to revoke permission for Alice to access their data...")
  let cancel_alice = await bob.call(
    "radvue",
    "delete_cap_grant",
    bob_grant_header
  );
  
  // Bob has now cancelled the CapGrant. Let's make sure Alice can't use it.
  // test #11 - ensure we have a valid error when call_cap_claim is executed with the revoked CapGrant.
  try {
  let TokenResults2 = await alice.call(
    "radvue",
    "call_list_cap_claim",
    bob_happ.agent
  );

  console.log("Try to use the deleted cap grant:")
  console.log(TokenResults2);

  } catch (err) {
  testcount++;
  t.ok(err,testcount+": Alice cannot use revoked CapGrant")
  };

  // Bob is giving Carol capGrant to access create_health_data
  console.log("CREATE DATA WITH CAPGRANT");
  console.log("Now Bob is going to give Carol delegation to create data...")
  let AssignCreateGrant = await bob.call(
    "radvue",
    "create_capgrant",
    {
      function: "create_health_data_with_capgrant",
      agent: carol_happ.agent,
    }
  );

  // get cap tokens for Carol
  let CarolAccess = await carol.call(
    "radvue",
    "get_cap_tokens",
    bob_happ.agent
  );

  console.log("Now Carol is going to use that token to create an entry for Bob...")
  let CarolCreate1 = await carol.call(
  "radvue",
  "call_create_cap_claim",
  {
    filterByAgent: bob_happ.agent,
    content: "Bob #3",
    resourceType: "Urology",
    startTime: 1629973116813,
    endTime: 1629973116813
  }
);

await sleep(500);

  console.log("List the results of Carol's call using the capability token...")
  // log response from remote call
  console.log(CarolCreate1);
  // test #10 - do we have a valid response?
  testcount++;
  t.ok(CarolCreate1,testcount+": Carol has a valid response from CapGrant");

  // get bob to list all their audit entries
  console.log("Now get Bob to list his audit data entries yet again...")
  let list_audit_data_bob3 = await bob.call(
    "radvue",
    "list_audit_data",
    null
  );
  console.log(list_audit_data_bob3);

  // verify there are now 2 entries
  console.log("Confirm there are now 2 audit entries for Bob");
  testcount++;
  t.equal(list_audit_data_bob3.length,2,testcount+": Bob now has 2 Audit Data entries");

  // get bob to list all their health data entries - should now have 3
  console.log("Now get Bob to list his health data entries yet again, including Carol's new one...")
  // get bob to list all his entries
  const list_health_data_bob_2 = await bob.call(
    "radvue",
    "list_health_data",
    null
  );
  console.log(list_health_data_bob_2);
  testcount++;
  t.equal(list_health_data_bob_2.length,3,testcount+": Bob has 3 Health Data entries");

  // get carol to list all their health data entries
  console.log("CAROL_LIST_DATA_2")
  // get bob to list all his entries
  const list_health_data_carol_2 = await carol.call(
    "radvue",
    "list_health_data",
    null
  );
  console.log("LIST_HEALTH_DATA_CAROL_2", list_health_data_carol_2.length);
  console.log(list_health_data_carol_2);


  // verify that Carol cannot read Bob's entries
  try {
    let TokenResults4 = await carol.call(
      "radvue",
      "call_list_cap_claim",
      bob_happ.agent
    );
  
    console.log("Carol tries to list Bob's data without a capgrant:");
    console.log(TokenResults4);
  
    } catch (err) {
    testcount++;
    t.ok(err,testcount+": Yaay Carol cannot access Bob's Health Data entries");
    };


  // Bob is giving Carol capGrant to access list_health_data
  console.log("CAROL LIST DATA WITH CAPGRANT");
  console.log("Now Bob is going to give Carol delegation to list data...");
  let AssignCreateGrant2 = await bob.call(
    "radvue",
    "create_capgrant",
    {
      function: "list_health_data_with_capgrant",
      agent: carol_happ.agent,
    }
  );

  // Carol uses grant to list health data
  console.log("Now Carol is going to use that token to get Bob's entries...");
  let CarolList1 = await carol.call(
    "radvue",
    "call_list_cap_claim",
    bob_happ.agent
  );

  console.log("List the results of Carol's call using the capability token...");
  // log response from remote call
  console.log(CarolList1);
  // test #8 - do we have a valid response?
  testcount++;
  t.ok(CarolList1,testcount+": Carol has a valid response from CapGrant");

});