use crate::audit_data;
use crate::capgrant;
use crate::utils;

use hdk::prelude::*;
use chrono::{serde::ts_milliseconds, DateTime, Utc};

#[hdk_entry(id = "health_data", visibility = "private")]
#[serde(rename_all = "camelCase")]
#[derive(Clone)]
pub struct HealthData {
    pub created_by: AgentPubKey,
    pub content: String,
    pub resource_type: String,
    #[serde(with = "ts_milliseconds")]
    pub start_time: DateTime<Utc>,
    #[serde(with = "ts_milliseconds")]
    pub end_time: DateTime<Utc>,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct HealthDataInput {
    pub content: String,
    pub resource_type: String,
    #[serde(with = "ts_milliseconds")]
    pub start_time: DateTime<Utc>,
    #[serde(with = "ts_milliseconds")]
    pub end_time: DateTime<Utc>,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct HealthDataRemoteInput {
    pub filter_by_agent: AgentPubKey,
    pub content: String,
    pub resource_type: String,
    #[serde(with = "ts_milliseconds")]
    pub start_time: DateTime<Utc>,
    #[serde(with = "ts_milliseconds")]
    pub end_time: DateTime<Utc>,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct HealthDataOutput {
    wrapper_entry_hash: EntryHash,
    entry: HealthData,
}

#[hdk_entry(id = "health_data_wrapper", visibility = "public")]
#[derive(Clone)]
pub struct HealthDataWrapper {
    pub entry_hash: EntryHash,
}

/// Creates a new health_data entry.
/// 
/// Entries are private and exist only on source chain of agent. Output is full entry plus a wrapper entry hash.
/// 
/// The wrapper entry hash is public and is used so that entries can be pushed to the DHT.
/// However, only authorized users can get to the underlying entry in the source chain.
pub fn create_health_data(health_data_input: HealthDataInput,) -> ExternResult<HealthDataOutput> {
    let agent_info = agent_info()?;

    debug!("##################CREATE_HEALTH_DATA_AGENT_INFO{:?}",agent_info);
 
    let health_data = HealthData {
        created_by: AgentPubKey::from(agent_info.agent_latest_pubkey.clone()),
        content: health_data_input.content,
        resource_type: health_data_input.resource_type,
        start_time: health_data_input.start_time,
        end_time: health_data_input.end_time
    };

    debug!("##################CREATE_HEALTH_DATA_INPUT{:?}",health_data);
 
    create_entry(&health_data)?;
 
    let health_data_hash = hash_entry(&health_data)?;

    let health_data_wrapper = HealthDataWrapper {
        entry_hash: EntryHash::from(health_data_hash.clone())
    };
    create_entry(&health_data_wrapper)?;

    let health_data_wrapper_hash = hash_entry(&health_data_wrapper)?;
 
    let path = health_data_path(health_data.created_by.clone());
    debug!("##################CREATE_HEALTH_DATA_PATH{:?}",path);
 
    path.ensure()?;
 
    create_link(path.hash()?, health_data_wrapper_hash.clone(), ())?;
 
    Ok( HealthDataOutput{
        wrapper_entry_hash: health_data_wrapper_hash,
        entry: health_data,
    })
}

/// Returns all health_data entries for the calling user.
/// 
/// Requires no input and will just return a vector of every health data entry.
pub fn list_health_data() -> ExternResult<Vec<HealthDataOutput>> {
    let path = health_data_path(AgentPubKey::from(agent_info()?.agent_latest_pubkey.clone()));
    debug!("##################LIST_HEALTH_DATA_PATH{:?}",path);
    let links = get_links(path.hash()?, None)?;
    debug!("##################LIST_HEALTH_DATA_LINKS{:?}",links);

    let mut results = vec![];
    for a in links
        .iter()
        {
            debug!("##################try get and convert1: {:?}",a.target.clone());
            let wrapper = utils::try_get_and_convert::<HealthDataWrapper>(HoloHash::from(a.target.clone()))?;
            debug!("##################try get and convert2: {:?}",wrapper.entry_hash);
            let health_data = utils::try_get_and_convert::<HealthData>(wrapper.entry_hash)?;
            debug!("##################try get and convert3: {:?}",health_data);
            results.push(HealthDataOutput{
                wrapper_entry_hash:HoloHash::from(a.target.clone()),
                entry:health_data
            })
        }
        return Ok(results);
}


/// Allows a CapGrant holder to write a Health Data entry to a users chain AND simultaneously writes an audit-data entry to the grantor's source chain.
/// 
/// This can only take one argument if we are exposing to the host via hdk_extern (which is required because the tests all fail otherwise :/)
pub fn create_health_data_with_capgrant(health_data: HealthData) -> ExternResult<HealthDataOutput> {

    // let agent_info = agent_info()?;
    let create_health_data = HealthData {
        created_by: health_data.created_by.clone(),
        // created_by: AgentPubKey::from(agent_info()?.agent_latest_pubkey.clone()),
        content: health_data.content,
        resource_type: health_data.resource_type.clone(),
        start_time: health_data.start_time,
        end_time: health_data.end_time
    };
 
    debug!("##################CREATE_HEALTH_DATA_WITH_CAPGRANT_INPUT{:?}",create_health_data);

    create_entry(&create_health_data)?;

    // // write audit data at the same time
    
    let now = sys_time()?;
    debug!("### create_health_data_with_capgrant_timestamp {:#?}",now);

    let audit_data_input = audit_data::AuditData {
        created_by: health_data.created_by.clone(),
        record_header: "CREATE DATA".to_string(),
        resource_type: health_data.resource_type.clone(), // return a resource type to see what the grantee created
        timestamp: now.to_string(),
    };

    create_entry(&audit_data_input)?;
 
    let audit_data_hash = hash_entry(&audit_data_input)?;

    let audit_data_wrapper = audit_data::AuditDataWrapper {
        entry_hash: EntryHash::from(audit_data_hash.clone())
    };
    create_entry(&audit_data_wrapper)?;

    let audit_data_wrapper_hash = hash_entry(&audit_data_wrapper)?;
 
    let path = audit_data::audit_data_path(AgentPubKey::from(agent_info()?.agent_latest_pubkey.clone()));
    debug!("##################LIST_HEALTH_DATA_CAPGRANT_PATH2{:?}",path);
 
    path.ensure()?;
 
    create_link(path.hash()?, audit_data_wrapper_hash.clone(), ())?;

    // finish writing audit data
    
    let health_data_hash = hash_entry(&create_health_data)?;

    let health_data_wrapper = HealthDataWrapper {
        entry_hash: EntryHash::from(health_data_hash.clone())
    };
    create_entry(&health_data_wrapper)?;

    let health_data_wrapper_hash = hash_entry(&health_data_wrapper)?;
 
    let path = health_data_path(AgentPubKey::from(agent_info()?.agent_latest_pubkey.clone()));
    debug!("##################CREATE_HEALTH_DATA_PATH{:?}",path);
 
    path.ensure()?;
 
    create_link(path.hash()?, health_data_wrapper_hash.clone(), ())?;
 
    Ok( HealthDataOutput{
        wrapper_entry_hash: health_data_wrapper_hash,
        entry: create_health_data,
    })
}

/// Allows a CapGrant holder to list all entries in the grantors source chain AND simultaneously writes an audit-data entry to the grantor's source chain.
pub fn list_health_data_remote(cap_for: capgrant::CapReceive,pubkey: AgentPubKey) -> ExternResult<Vec<HealthDataOutput>> {
    let remote_result = call_remote(
        cap_for.from_agent,
        zome_info()?.name,
        "list_health_data_with_capgrant".to_string().into(),
        Some(cap_for.cap_secret),
        pubkey.clone(),
    )?;
    debug!("### capfor: {:#?}",cap_for.cap_secret);
    debug!("### zome: {:#?}",zome_info()?.name);
    debug!("### list_health_data_remote result: {:#?}",remote_result);
    debug!("### pubkey: {:#?}",pubkey.clone());
    // If the call_remote call was successfull, we'll get an Ok variant
    if let ZomeCallResponse::Ok(inner_io) = remote_result {
        // We convert the binary bytes values into our HealthDataOutput
        let health_data: Vec<HealthDataOutput> = inner_io.decode()?;
        Ok(health_data)
    } else {
        // and if it wasn't, return an error
        // TODO Handle all other ZomeCallResponse responses specifically
        Err(WasmError::Guest(String::from("Remote call was not successful, but I'm not sure why")))
    }
}

pub fn create_health_data_remote(cap_for: capgrant::CapReceive,health_data: HealthData) -> ExternResult<HealthDataOutput> {
    debug!("### create_health_data_remote cap_for: {:#?}",cap_for);
    debug!("### create_health_data_remote health_data: {:#?}",health_data);
    let remote_result = call_remote(
        cap_for.from_agent,
        zome_info()?.name,
        "create_health_data_with_capgrant".to_string().into(),
        Some(cap_for.cap_secret),
        health_data,
    )?;
    debug!("### create_health_data_remote result: {:#?}",remote_result);
    // If the call_remote call was successfull, we'll get an Ok variant
    if let ZomeCallResponse::Ok(inner_io) = remote_result {
        // We convert the binary bytes values into our HealthDataOutput
        let health_data: HealthDataOutput = inner_io.decode()?;
        Ok(health_data)
    } else {
        // and if it wasn't, return an error
        // TODO Handle all other ZomeCallResponse responses specifically
        Err(WasmError::Guest(String::from("Remote call was not successful, but I'm not sure why")))
    }
}

// returns all health_data entries when using CapGrant AND writes an audit-data entry
pub fn list_health_data_with_capgrant(pubkey: AgentPubKey) -> ExternResult<Vec<HealthDataOutput>> {
    let path = health_data_path(AgentPubKey::from(agent_info()?.agent_latest_pubkey.clone()));

    let links = get_links(path.hash()?, None)?;

    // ### write audit data at the same time
    
    let now = sys_time()?;
    debug!("### list_health_data_timestamp: {:#?}",now);

    let audit_data_input = audit_data::AuditData {
        created_by: pubkey,
        record_header: "LIST DATA".to_string(),
        resource_type: "Pending".to_string(), // placeholder to filter grants by categories of data
        timestamp: now.to_string(),
    };
    // TODO: Use resource_type for categorisation of data which can be used for capgrant process

    debug!("### audit_data_input: {:#?}",audit_data_input);

    create_entry(&audit_data_input)?;
 
    let audit_data_hash = hash_entry(&audit_data_input)?;

    let audit_data_wrapper = audit_data::AuditDataWrapper {
        entry_hash: EntryHash::from(audit_data_hash.clone())
    };
    create_entry(&audit_data_wrapper)?;

    let audit_data_wrapper_hash = hash_entry(&audit_data_wrapper)?;

    let path = audit_data::audit_data_path(AgentPubKey::from(agent_info()?.agent_latest_pubkey.clone()));
    debug!("##################LIST_HEALTH_DATA_CAPGRANT_PATH2{:?}",path);

    path.ensure()?;
 
    create_link(path.hash()?, audit_data_wrapper_hash.clone(), ())?;
    
    // ### finish writing audit data

    let mut results = vec![];
    for a in links
        .iter()
        {
            debug!("##################try get and convert1: {:?}",a.target.clone());
            let wrapper = utils::try_get_and_convert::<HealthDataWrapper>(HoloHash::from(a.target.clone()))?;
            debug!("##################try get and convert2: {:?}",wrapper.entry_hash);
            let health_data = utils::try_get_and_convert::<HealthData>(wrapper.entry_hash)?;
            debug!("##################try get and convert3: {:?}",health_data);
            results.push(HealthDataOutput{wrapper_entry_hash:HoloHash::from(a.target.clone()),entry:health_data})
        }
        return Ok(results);
}  

// Private helpers - anchors for the path
pub fn health_data_path(key: AgentPubKey) -> Path {
    Path::from(format!("health_data-{}",key))
}