use crate::utils;

use holo_hash::{AgentPubKey,EntryHash};
use hdk::prelude::*;


#[hdk_entry(id = "audit_data", visibility = "private")]
#[serde(rename_all = "camelCase")]
#[derive(Clone)]
pub struct AuditData {
    pub created_by: AgentPubKey,
    pub record_header: String,
    pub resource_type: String,
    pub timestamp: String,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AuditDataInput {
   pub record_header: String,
   pub resource_type: String,
   pub timestamp: String,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AuditDataOutput {
   wrapper_entry_hash: EntryHash,
   entry: AuditData,
}

#[hdk_entry(id = "audit_data_wrapper", visibility = "public")]
#[derive(Clone)]
pub struct AuditDataWrapper {
    pub entry_hash: EntryHash,
}

// returns all audit_data entries
pub fn list_audit_data() -> ExternResult<Vec<AuditDataOutput>> {
    let path = audit_data_path(AgentPubKey::from(agent_info()?.agent_latest_pubkey.clone()));
    debug!("##################LIST_AUDIT_DATA_PATH{:?}",path);
    let links = get_links(path.hash()?, None)?;
    debug!("##################LIST_AUDIT_DATA_LINKS{:?}",links);
    
    let mut results = vec![];
    for a in links
        // .into_inner()
        .iter()
        {
            debug!("##################try get and convert1: {:?}",a.target.clone());
            let wrapper = utils::try_get_and_convert::<AuditDataWrapper>(HoloHash::from(a.target.clone()))?;
            debug!("##################try get and convert2: {:?}",wrapper.entry_hash);
            let audit_data = utils::try_get_and_convert::<AuditData>(wrapper.entry_hash)?;
            debug!("##################try get and convert3: {:?}",audit_data);
            results.push(AuditDataOutput{wrapper_entry_hash:HoloHash::from(a.target.clone()),entry:audit_data})
        }
        return Ok(results);
}

// Private helpers - anchors for the path
pub fn audit_data_path(key: AgentPubKey) -> Path {
    Path::from(format!("audit_data-{}",key))
}