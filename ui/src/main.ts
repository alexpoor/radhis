import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import moshaToast from 'mosha-vue-toastify'
import 'mosha-vue-toastify/dist/style.css'
import { FontAwesomeIcon } from './plugins/font-awesome'
import vfmPlugin from 'vue-final-modal'



const radvue = createApp(App)
radvue.use(moshaToast)
radvue
.use(vfmPlugin)
.use(store)
.use(router)
.component("font-awesome-icon", FontAwesomeIcon)

radvue.mount('#app')


// createApp(App).use(store).use(router).mount('#app')