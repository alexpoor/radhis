use hdk::prelude::*;
use crate::utils;

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ProfileInput {
    pub nickname: String,
}

#[hdk_entry(id = "profile", visibility = "public")]
#[serde(rename_all = "camelCase")]
#[derive(Clone)]
pub struct Profile {
    pub player_id: AgentPubKey,
    pub nickname: String,
}

// output struct
#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ProfileOutput {
   entry_hash: EntryHash,
   entry: Profile,
}

/// Creates a PlayerProfile instance, commits it as a Holochain entry
pub fn create_profile(profile_input: ProfileInput) -> ExternResult<ProfileOutput> {
    let agent_info = agent_info()?;
    let profile = Profile {
        player_id: AgentPubKey::from(agent_info.agent_latest_pubkey.clone()),
        nickname: profile_input.nickname,
    };
    create_entry(&profile)?;

    let profile_hash = hash_entry(&profile)?;

    let path = profile_path();

    path.ensure()?;

    create_link(path.hash()?, profile_hash.clone(), ())?;

    Ok( ProfileOutput{
        entry_hash: EntryHash::from(profile_hash),
        entry: profile,
    })

}

// returns all profile entries
pub fn list_profiles() -> ExternResult<Vec<ProfileOutput>> {
    let path = profile_path();
    debug!("##################LIST_HEALTH_DATA_PATH{:?}",path);
    let links = get_links(path.hash()?, None)?;
    debug!("##################LIST_HEALTH_DATA_LINKS{:?}",links);
    let mut results = vec![];
    for a in links
        .iter()
        {
            let profile = utils::try_get_and_convert::<Profile>(HoloHash::from(a.target.clone()))?;
            debug!("##################try get and convert3: {:?}",profile);
            results.push(ProfileOutput{
                entry_hash:HoloHash::from(a.target.clone()),
                entry:profile
            })
        }
        return Ok(results);
}

// Private helpers - anchors for the path
pub fn profile_path() -> Path {
    Path::from(format!("profile"))
}