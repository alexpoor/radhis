# Requirements for UI
This is an attempt to document UI requirements for the RADHIS project.
Some of this has already been implemented in a draft VueJS project (found in the `ui` folder [here](https://gitlab.com/alexpoor/radhis)).

## Functional requirements
1. User can create new health data entries
2. User can view their health data entries
3. User can select another user to delegate access to. This could be:
    - Permission to view their entries
    - Permission to create new entries for them
4. Delegate can now see the grantors entries, or create entries for them
5. The grantor can access audit data, on what the delegate has done (`list_health_data` or `create_health_data` events)
6. Grantor can revoke the permissions.

## Components
- A form to create data
- A table object to view data
- Access delegation widget:
    - Select who you are giving access to
    - Select which permissions to delegate
- A form to create data as delegate (form elements are the same)
- A table object to view delegated data (table elements are the same)
- A table object to view audit data
- A widget to revoke delegated permissions.

## Creating data [implemented]
Currenty the backend requires four elements to be posted for a valid entry. The function to use is `create_health_data`. Required elements (shown with Rust data types) are:
- `content: String`: The substance of the health data entry - what actually occurred? This could be free text data, or structured data submitted as a JSON object (such as FHIR).
- `resourceType: String`: Some categorisation of the event. This could be based on medical specialty (eg. Surgery or Orthopaedics), or perhaps 'User-generated health data' (eg. Fitbit).
- `startTime: DateTime<Utc>`: The start of the event. Must be sent to backend in UNIX time as an int.
- `endTime: DateTime<Utc>`: The end of the event. Must be sent to backend in UNIX time as an int.

Since the backend only accepts a UNIX time int, the front end requires a date picker which can represent the datetime in a user-friendly way. This will have to be converted to UNIX time before sending to the backend.

The `create_health_data` function can be called as follows:
```js
await appWs.callZome({
            cap: null,
            cell_id: cell_id,
            zome_name: 'radvue',
            fn_name: 'create_health_data',
            payload: {
                content: this.hdf.content,
                resourceType: this.hdf.resourcetype,
                startTime: parseInt(this.hdf.starttime),
                endTime: parseInt(this.hdf.endtime),
            },
            provenance: cell_id[1],
        });
```

and would return a response like so:
```json
Object { wrapperEntryHash: Uint8Array(39), entry: {…} }
    entry: Object { content: "second", resourceType: "post", startTime: 222, … }
        content: "second"
        createdBy: Uint8Array(39) [ 132, 32, 36, … ]
        endTime: 222
        resourceType: "post"
        startTime: 222
        <prototype>: Object { … }
    wrapperEntryHash: Uint8Array(39) [ 132, 33, 36, … ]
```

`wrapperEntryHash` is a unique identifier for that entry on the DHT, stored as a Uint8Array which can easily be decoded to Base64 if required. It can be used in VueJS as a key for iterating through responses when populating a table.

`createdBy` is a unique identifier for the agent that published the entry, also stored as a Uint8Array. Note that where permissions have been delegated to create data, it is the delegate's identifier that will be stored here rather than the grantor.

## Viewing data [implemented]
A user who wishes to see all their data, can do so via the `list_health_data` function in the backend. This requires no payload, so an example UI call would be:
```js
const res = await appWs.callZome({
          cap: null,
          cell_id: cell_id,
          zome_name: 'radvue',
          fn_name: 'list_health_data',
          payload: null,
          provenance: cell_id[1],
        });
```

It will return a JSON array of all entries currently in that user's source chain.
Accessing parts of the response, such as the `content` of the second entry, can be done like so (where the response has already been passed to `res`):
```js
console.log(res[1].entry.content);
```

Viewing data in this way will only ever return the current user's source chain entries.

## Delegating access [not implemented in UI]
If users want to delegate access to their data, there are two steps:
1. Select the user you want to delegate to. Functionally this requires a way to: see who all the available users are, and then pick one. The selected user will have their unique identifier passed into the relevant backend function. This obviously requires that *all* users are known and accessible to everyone's instance, and there is no backend provision for this currently.
2. Select which access to delegate. There are two methods: View data, or Create data. A user might want to do both.

Once the above steps are completed, the following workflow needs to be executed.

### Create cap grant
Person delegating access creates a 'Capability Grant'. This is done via `create_capgrant` function, and requires a payload of:
* ``function`` - the specific function being enabled for the delegate. This can either be `list_health_data_with_capgrant` to provide list access, or `create_health_data_with_capgrant` to provide entry creation access.
* ``agent`` - the ID Of the delegate being given access. **TODO: Figure out how to access this!**

An example call would look like:
```js
await appWs.callZome({
            cap: null,
            cell_id: cell_id,
            zome_name: 'radvue',
            fn_name: 'create_capgrant',
            payload: {
                function: "list_health_data_with_capgrant",
                agent: DelegatePubKey,
            },
            provenance: cell_id[1],
        });
```

### Get cap tokens
Now the grant has been made, the delegate needs to request a token from the grantor. This is done with the `get_cap_tokens` which only requires the ID of the grantor in the payload. This call will simply get all tokens available from the specified user.

An example would be:
```js
await appWs.callZome({
            cap: null,
            cell_id: cell_id,
            zome_name: 'radvue',
            fn_name: 'get_cap_tokens',
            payload: GrantorPubKey,
            provenance: cell_id[1],
        });
```

This call writes the token to the delegates source chain.

## Using capability tokens [not implemented in UI]
As mentioned, there are two different workflows for use of a capability token - read or create. The two relevant public functions available are `call_create_cap_claim` and `call_list_cap_claim`. Both have a similar workflow in the backend:
* Tokens are retrieved
* These are passed to another function, which collects the tokens and the user ID
* The ID and tokens are used to call another function, which actually executes the desired response.

### `call_list_cap_claim`
Requires only the agent/user ID in the payload. **NOTE**: the agent/user ID should be that of the *grantor*, not the delegate! An example call would look like:
```js
await appWs.callZome({
            cap: null,
            cell_id: cell_id,
            zome_name: 'radvue',
            fn_name: 'call_list_cap_claim',
            payload: GrantorPubKey,
            provenance: cell_id[1],
        });
```

### `call_create_cap_claim`
This is a bit more complex than the previous one. The entire `create entry` payload must be sent to this function, along with the agent ID of the grantor. An example call would look like:
```js
await appWs.callZome({
            cap: null,
            cell_id: cell_id,
            zome_name: 'radvue',
            fn_name: 'call_create_cap_claim',
            payload:   {
                filterByAgent: GrantorPubKey,
                content: "Bob;s data",
                resourceType: "Urology",
                startTime: 1629973116813,
                endTime: 1629973116813
            }
            provenance: cell_id[1],
        });
```

### Revoking capability grants
At any time, a user should be able to revoke a capability grant. There is no way currently for the delegate to know this has happened, other than trying to make a call using the grant and it being unauthorized. The call to make is very simple - however, the payload required is the HeaderHash entry for the original capability grant, so these do need to be retained somewhere in the app (or available via query). This HeaderHash value is available in the output from the previous `list_health_data_with_capgrant` or `create_health_data_with_capgrant` calls, and can be accessed from this object via something like:

```js

  const HeaderHashForDelete = CapGrant.header_hash;

  let CapGrant = await await appWs.callZome({
    cap: null,
            cell_id: cell_id,
            zome_name: 'radvue',
            fn_name: 'delete_cap_grant',
            payload: HeaderHashForDelete,
            provenance: cell_id[1],
        });
  );
```

A user might have issued a number of cap grants to different users and so, ideally, there should be a facility to pick and choose what you want to do with each. However, this functionality is not required for this MVP and so a 'Revoke all' action would be quicker to develop, and just as effective.

## Audit data [not implemented in UI]
Audit data is data created whenever a capability grant is used. Capability grants allow you to take actions, as if you were another user. Therefore, if Alice has given Bob a cap grant to read her data, Bob effectively calls the `list_health_data` function - using the cap token - *as if he were Alice*.

For health data we will want to know who and when has accessed our data in this fashion, and this is where audit data comes in.

Any time that `list_health_data_with_capgrant` or `create_health_data_with_capgrant` are used, the function automatically makes a separate call in the backend to write an audit data entry on the grantor's source chain. The structure of audit data is as follows:

* `createdBy: string`: the ID of the delegate who used the cap token
* `recordHeader: string`: the action taken by the delegate (eg. LIST or CREATE)
* `resourceType: string`: counterpart of `resourceType` in `create_health_data`. Could allow searching by resource type in future (eg. who has accessed any records related to Sexual Health, or Mental Health)
* `timestamp: string`: a timestamp of when the data was accessed or created.

A user at any time should be able to list these audit entries, to check how a capability grant has been used. This is done via a `list_audit_data` function. Like `list_health_data`, it requires no input parameter and will return an array. It can be called like so:
```js
await appWs.callZome({
            cap: null,
            cell_id: cell_id,
            zome_name: 'radvue',
            fn_name: 'list_audit_data',
            payload: null,
            provenance: cell_id[1],
        });
```

## Key questions
- How can a user know what other users there are? If there is some 'player profile' function, does it mean some kind of 'sign up' page is needed??
- How can a delegate know who has issued a cap grant for them, so they can make a call for cap tokens? (or, what is payload for `call_list_cap_claim`?)
- The HeaderHash of the cap grant is required to revoke a cap claim. How can this be retained and made available?
- How can we quickly demo multiple local agents with existing UI?
- What is the process to use a proxy and demo multiple remote agents using the app?
- How can I select a username and then get the ID/agentpubkey for that user to send to the backend?
    - How can I reliably get the ID/agentpubkey as UInt8Array?